#!/bin/bash
# Server installation wrapper

#
# Currently Supported Operating Systems:
#
#   Debian
#

# Am I root?
if [ "x$(id -u)" != 'x0' ]; then
  echo 'Error: this script can only be executed by root'
  exit 1
fi

# Detect OS
case $(head -n1 /etc/issue | cut -f 1 -d ' ') in
  Debian) type="debian" ;;
esac

# Check wget
if [ -e '/usr/bin/wget' ]; then
  wget -qL https://gitlab.com/kd-works/scripts/raw/main/unix/auto/config/server/distrib/$type/install.sh -O install-$type.sh

  if [ "$?" -eq '0' ]; then
    bash install-$type.sh $*
    exit
  else
    echo "Error: install-$type.sh download failed."
    exit 1
  fi
fi

# Check curl
if [ -e '/usr/bin/curl' ]; then
  curl -sSL https://gitlab.com/kd-works/scripts/raw/main/unix/auto/config/server/distrib/$type/install.sh -o install-$type.sh

  if [ "$?" -eq '0' ]; then
    bash install-$type.sh $*
    exit
  else
    echo "Error: install-$type.sh download failed."
    exit 1
  fi
fi

exit
