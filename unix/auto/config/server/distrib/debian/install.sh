#!/bin/bash
# Server installation wrapper

#
# Currently Supported Operating Systems:
#
#   Debian 9
#

# Am I root?
if [ "x$(id -u)" != 'x0' ]; then
  echo 'Error: this script can only be executed by root'
  exit 1
fi

# Detect version
case $(head -n1 /etc/debian_version | cut -f 1 -d '.') in
  9) version="9" ;;
esac

# Check wget
if [ -e '/usr/bin/wget' ]; then
  wget -qL https://gitlab.com/kd-works/scripts/raw/main/unix/auto/config/server/distrib/debian/versions/$version/install.sh -O install-debian-$version.sh

  if [ "$?" -eq '0' ]; then
    bash install-debian-$version.sh $*
    exit
  else
    echo "Error: install-debian-$version.sh download failed."
    exit 1
  fi
fi

# Check curl
if [ -e '/usr/bin/curl' ]; then
  curl -sSL https://gitlab.com/kd-works/scripts/raw/main/unix/auto/config/server/distrib/debian/versions/$version/install.sh -o install-debian-$version.sh

  if [ "$?" -eq '0' ]; then
    bash install-debian-$version.sh $*
    exit
  else
    echo "Error: install-debian-$version.sh download failed."
    exit 1
  fi
fi

exit
