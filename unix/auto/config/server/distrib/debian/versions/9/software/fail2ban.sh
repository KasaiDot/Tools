#!/bin/bash

install() {
    apt -y install fail2ban
}

config() {
    sudo cp /etc/fail2ban/fail2ban.conf /etc/fail2ban/fail2ban.local
    sudo cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local

    sudo fail2ban-client set JAIL addignoreip $MY_IP

    # sed -i "s+ignoreip = 127.0.0.1\/8+ignoreip = 127.0.0.1\/8 $MY_IP+g" /etc/fail2ban/jail.local
}

init() {
    install

    config

    service fail2ban restart
}

init
