#!/bin/bash

# Auto Server installer v.01

#----------------------------------------------------------#
#                Variables & Functions                     #
#----------------------------------------------------------#

VERSION='debian'
memory=$(grep 'MemTotal' /proc/meminfo |tr ' ' '\n' |grep [0-9])
arch=$(uname -i)
os='debian'
release=$(cat /etc/debian_version|grep -o [0-9]|head -n1)
codename="$(cat /etc/os-release |grep VERSION= |cut -f 2 -d \(|cut -f 1 -d \))"

help() {
    echo "Usage: $0 [OPTIONS]
  -c, --clamav            Install ClamAV        [yes|no]  default: yes
  -i, --iptables          Install Iptables      [yes|no]  default: yes
  -b, --fail2ban          Install Fail2ban      [yes|no]  default: yes
  -l, --lang              Default language                default: en
  -y, --interactive       Interactive install   [yes|no]  default: yes
  -s, --hostname          Set hostname
  -f, --force             Force installation
  -h, --help              Print this help

  Example: bash $0 -e demo@vestacp.com -p p4ssw0rd --apache no --phpfpm yes"
  exit 1
}

##

#----------------------------------------------------------#
#                       Brief Info                         #
#----------------------------------------------------------#

# Printing nice ASCII logo
clear

echo "  _  __               _   "
echo " | |/ /__ _ ___  __ _(_)  "
echo " | ' // _` / __|/ _` | |  "
echo " | . \ (_| \__ \ (_| | |_ "
echo " |_|\_\__,_|___/\__,_|_(_)"
echo
echo "                    Kasai."
echo -e "\n\n"

echo 'The following software will be installed on your system:'

# Firewall stack
if [ "$iptables" = 'yes' ]; then
    echo -n '   - Iptables Firewall'
fi
if [ "$iptables" = 'yes' ] && [ "$fail2ban" = 'yes' ]; then
    echo -n ' + Fail2Ban'
fi
echo -e "\n\n"
