#!/bin/bash

if [ "$(whoami)" != "root" ]; then
    SUDO=sudo
fi

APT=${SUDO} apt -y
INSTALL=${APT} install

echo "DAEMON SETUP"

read -p 'Domain Name (panel.example.com): ' domain
read -p 'NodeJS Version (8, 9, 10, etc...)' NODEJS_VERSION

server:update() {
    ${APT} update
    ${APT} upgrade
    ${APT} autoremove
}

setup:nodejs() {
    curl -sL https://deb.nodesource.com/setup_${NODEJS_VERSION}.x | ${SUDO} -E bash -
    ${INSTALL} nodejs make gcc g++
    ${SUDO} npm i -g npm@latest
}

setup:daemon() {
    mkdir -p /srv/daemon /srv/daemon-data
    cd /srv/daemon
    curl -L https://github.com/pterodactyl/daemon/releases/download/v0.6.12/daemon.tar.gz | ${SUDO} tar --strip-components=1 -xzv
    ${SUDO} npm install --only=production

    clear
    echo -e ""
    echo -e "! configure node on the web !"
    echo -e "! enter the token of your node !"
    read -p 'Token: ' token
    clear

    ${SUDO} npm run configure -- --panel-url https://$domain --token $token
    ${SUDO} npm start
}

setup:docker() {
    curl -fsSL https://download.docker.com/linux/debian/gpg | ${SUDO} apt-key add -
    ${SUDO} add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"

    server:update

    ${INSTALL} docker-ce
    ${SUDO} systemctl enable docker
}

setup:wings() {
    echo -e "[Unit]\nDescription=Pterodactyl Wings Daemon\nAfter=docker.service\n\n[Service]\nUser=root\n#Group=some_group\nWorkingDirectory=/srv/daemon\nLimitNOFILE=4096\nPIDFile=/var/run/wings/daemon.pid\nExecStart=/usr/bin/node /srv/daemon/src/index.js\nRestart=on-failure\nStartLimitInterval=600\n\n[Install]\nWantedBy=multi-user.target" > /etc/systemd/system/wings.service

    ${SUDO} systemctl enable --now wings
}

server:update

setup:nodejs
setup:daemon
setup:docker
setup:wings

echo "Done!"
read -n 1 -s

reboot
