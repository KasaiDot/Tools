#!/bin/bash

if [ "$(whoami)" != "root" ]; then
    SUDO=sudo
fi

APT=${SUDO} apt -y
INSTALL=${APT} install

echo "SETUP"

read -p "PHP VERSION ($VERSION_PHP_AVAILABLE): " VERSION_PHP
read -p "MYSQL VERSION ($VERSION_MYSQL_AVAILABLE): " VERSION_MYSQL

packages:add:php() {
   curl -sS https://packages.sury.org/php/README.txt | ${SUDO} bash
   server
}

packages:add:mysql() {
    ${SUDO} add-apt-repository "deb https://repo.mysql.com/apt/debian/ $(lsb_release -sc) mysql-$VERSION_MYSQL"
    server
}

packages:add:mariadb() {
    curl -sS https://downloads.mariadb.com/MariaDB/mariadb_repo_setup | ${SUDO} bash
    server
}

packages:install:php() {
    ${INSTALL} php$VERSION_PHP php$VERSION_PHP-mysql php$VERSION_PHP-gd php$VERSION_PHP-ldap php$VERSION_PHP-curl php$VERSION_PHP-cli php$VERSION_PHP-mcrypt php$VERSION_PHP-soap php$VERSION_PHP-xml
    server
}

packages:install:mysql() {
    ${INSTALL} mysql-server
    server
}

packages:install:mariadb() {
    ${INSTALL} mariadb-server
    server
}

packages:install:nginx() {
    ${INSTALL} nginx
    server
}

packages:install:apache() {
    ${INSTALL} apache2
    server
}

packages:install:phpmyadmin() {
    ${INSTALL} phpmyadmin
    server
}

packages:add() {
    packages:add:php
    packages:add:mysql
    packages:add:mariadb
}

packages:install() {
    packages:install:php
    packages:install:mysql
    packages:install:mariadb
    packages:install:nginx
    packages:install:apache
    packages:install:phpmyadmin
}

packages() {
    packages:add
    packages:install
}

packages
