#!/bin/bash

read -p "PROCESSMAKER VERSION (latest, 3.3.0, etc...): " VERSION_PROCESSMAKER

if [ $VERSION_PROCESSMAKER = "latest" ]; then
  curl -sSL "https://sourceforge.net/projects/processmaker/files/latest/download" -o "processmaker-$VERSION_PROCESSMAKER-community.tar.gz"
else
  curl -sSL "https://sourceforge.net/projects/processmaker/files/ProcessMaker/$VERSION_PROCESSMAKER/processmaker-$VERSION_PROCESSMAKER-community.tar.gz/download" -o "processmaker-community.tar.gz"
fi
