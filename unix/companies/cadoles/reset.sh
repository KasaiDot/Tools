#!/bin/bash

declare -a del=("~/Desktop/*" "~/Documents" "~/formation" "~/Images" "~/Modèles" "~/Musique" "~/Public" "~/Téléchargements" "~/Vidéos" "~/.atom" "~/.bash_history" "~/.cache" "~/.config" "~/.gconf" "~/.ICEauthority" "~/.lastpass" "~/.local" "~/.mozilla" "~/.nano" "~/.pki" "~/.ssh" "~/.sudo_as_admin_successful" "~/.thunderbird" "~/.vscode" "~/.filezilla")
declare -a notdel=("pitaya-branch(pitaya-debian).vdi")
declare -a cre=("~/Documents" "~/Images" "~/Modèles" "~/Musique" "~/Public" "~/Téléchargements" "~/Vidéos")

echo -ne "\033]2;Bye Bye Cadoles!\007"

clear

for file in "${del[@]}"
do
    echo -e " * Suppression de \033[31m$file\033[0m en cours..."

    if [ -d "$file" ]; then
        rm -rf $file
    else
        if [ -f "$file" ]; then
            for delfile in "${notdel[@]}"
            do
                if [ "$file" = "$delfile" ]; then
                    # do nothing
                else
                    rm $file
                fi
            done
        else
            echo -e " * \033[31m$file\033[0m à déjà été supprimé !"
        fi
    fi

    echo -e " * Suppression de \033[31m$file\033[0m \033[32mterminé\033[0m."
done

for file in "${cre[@]}"
do
    if [ -d "$file" ]; then
        # do nothing
    else
        echo -e " * Création de \033[31m$file\033[0m en cours..."

        mkdir $file

        echo -e " * Création de \033[31m$file\033[0m \033[32mterminé\033[0m."
    fi
done
