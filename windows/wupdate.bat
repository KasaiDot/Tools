@echo off

title Enable / Disable Windows Update

fltmc >nul 2>&1 && (
  :question
    cls
    echo What do you want to do?
    set /p c=[start / stop] : 
    if /I "%c%"=="start" (goto :start)
    if /I "%c%"=="stop" (goto :stop)
    goto :question

  :start
    cls
    sc config wuauserv start=auto
    net start wuauserv
    goto :eol

  :stop
    cls
    sc config wuauserv start=disabled
    net stop wuauserv
    goto :eol
) || (
  cls
  echo Restart the script, i need the Administrator rights for working!
  ping localhost -n 7 >nul
)
