const aggregation = (baseClass, ...mixins) => {
  class base extends baseClass {
    constructor(...args) {
      super(...args)

      mixins.forEach(mixin => {
        copyProps(this, (new mixin))
      })
    }
  }

  let copyProps = (target, source) => {
    Object.getOwnPropertyNames(source)
      .concat(Object.getOwnPropertySymbols(source))
      .forEach(prop => {
        if (!prop.match(/^(?:constructor|prototype|arguments|caller|name|bind|call|apply|toString|length)$/))
          Object.defineProperty(target, prop, Object.getOwnPropertyDescriptor(source, prop));
      })
  }

  mixins.forEach(mixin => {
    copyProps(base.prototype, mixin.prototype)
    copyProps(base, mixin)
  })

  return base
}

class DiscordAPI {
  constructor() {
    this._version = 6
    this._ptb = false
    this._canary = false
  }

  login(token) {
    this._token = token
  }

  /**
   * @param {number} id
   */
  set version(id) {
    this._version = id
  }

  /**
   * @param {boolean} ptb
   */
  set ptb(ptb) {
    this._ptb = ptb
  }

  /**
   * @param {boolean} canary
   */
  set canary(canary) {
    this._canary = canary
  }

  /**
   * @param {string} token
   */
  set token(token) {
    this._token = token
  }

  /**
   * Make a request to the DiscordAPI
   *
   * @param {string} type Type of the request
   * @param {string} endpoint Endpoint of the request
   * @param {string} data JSON stringified data for the request
   */
  async _request(type, endpoint, data) {
    const prefix = this._ptb ? 'ptb.' : this._canary ? 'canary.' : ''

    return await fetch(`https://${prefix}discordapp.com/api/v${this._version}/${endpoint}`, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': this._token
      },
      method: type,
      body: data
    })
  }
}

class DiscordAPIMethods extends DiscordAPI {
  constructor() {
    super()
  }

  setGuild(guildID) {
    this._guildID = guildID
    this._gdmthURL = `guilds/${this._guildID}`
  }

  setChannel(channelID) {
    this._channelID = channelID
    this._chmthURL = `channels/${this._channelID}`
  }
}

class DiscordAPIMethods_Guild extends DiscordAPIMethods {}

class DiscordAPIMethods_Channel extends DiscordAPIMethods_Guild {
  constructor() {
    super()
  }

  async _get(id) {
    const res = await this._request('GET', `${this._gdmthURL}/channels`)
    const channels = await res.json()

    return channels.find(el => el.id === id)
  }

  async _create(data) {
    await this._request('POST', `${this._gdmthURL}/channels`, JSON.stringify(data))
  }

  async _update(id, data) {
    await this._request('PATCH', `channels/${id}`, JSON.stringify(data))
  }

  async delete(id) {
    await this._request('DELETE', `channels/${id}`)
  }

  async swap(id, id2) {
    const tmp = await this._get(id)
    const first = await this._get(id)
    const second = await this._get(id2)

    await this._update(first.id, { second })
    await this._update(second.id, { tmp })
  }

  async _settyping(id, time = 0) {
    const interval = 9000

    time = time - interval

    await this._request('POST', `channels/${id}/typing`)

    this._typinginterval = setTimeout(() => {
      if (time > 0) this._settyping(id, time)
    }, interval)
  }

  async _bulkdelete(id, messages) {
    await this._request('POST', `channels/${id}/messages/bulk-delete`, {
      messages: messages
    })
  }
}

class DiscordAPIMethods_VoiceChannel extends DiscordAPIMethods_Channel {
  async swap(id, id2) {
    const tmp = await this._get(id)
    const first = await this._get(id)
    const second = await this._get(id2)

    await this._update(first.id, { second })
    await this._update(second.id, { tmp })
  }
}

class DiscordAPIMethods_Message extends DiscordAPIMethods_Channel {
  constructor() {
    super()
  }

  async _get(id) {
    const res = await this._request('GET', `${this._chmthURL}/messages`)
    const messages = await res.json()

    return messages.find(el => el.id === id)
  }

  async _create(data) {
    await this._request('POST', `${this._chmthURL}/messages`, JSON.stringify(data))

    clearTimeout(this._typinginterval)
  }

  async _update(id, data) {
    await this._request('PATCH', `${this._chmthURL}/messages/${id}`, JSON.stringify(data))
  }

  async delete(id) {
    await this._request('DELETE', `${this._chmthURL}/messages/${id}`)
  }

  async swap(id, id2) {
    const tmp = await this._get(id)
    const first = await this._get(id)
    const second = await this._get(id2)

    await this._update(first.id, { content: second.content })
    await this._update(second.id, { content: tmp.content })
  }
}

class DiscordAPIMethods_EmbedMessage extends DiscordAPIMethods_Message {
  async swap(id, id2) {
    const tmp = await this._get(id)
    const first = await this._get(id)
    const second = await this._get(id2)

    await this._update(first.id, { embed: second.embeds[0] })
    await this._update(second.id, { embed: tmp.embeds[0] })
  }
}

class DiscordGuild extends DiscordAPIMethods_Guild {}

class DiscordChannel extends aggregation(DiscordGuild, DiscordAPIMethods_Channel) {
  async update(id, name) {
    await this._update(id, {
      name: name,
      nsfw: false,
      rate_limit_per_user: 0,
      topic: '',
      type: 0,
      user_limit: 0
    })
  }

  async create(name) {
    await this._create({ name: name })
  }

  async rename(name) {
    await this._update(this._channelID, { name: name })

    return this
  }

  async setTopic(topic) {
    await this._update(this._channelID, { topic: topic })

    return this
  }

  async setNSFW(bool) {
    this._update(this._channelID, { nsfw: bool })

    return this
  }

  async setTyping(time = 0) {
    this._settyping(this._channelID, time)
  }

  async bulkDelete(messages) {
    await this._bulkdelete(this._channelID, messages)
  }
}

class DiscordVoiceChannel extends aggregation(DiscordChannel, DiscordAPIMethods_VoiceChannel) {}

class DiscordMessage extends aggregation(DiscordChannel, DiscordAPIMethods_Message) {
  constructor() {
    super()

    this._content = ''
  }

  setContent(content) {
    this._content = content

    return this
  }

  async get(id) {
    await this._get(id)
  }

  async send() {
    await this._create({ content: this._content })
  }

  async update(id) {
    await this._update(id, { content: this._content })
  }
}

class DiscordEmbedMessage extends aggregation(DiscordMessage, DiscordAPIMethods_EmbedMessage) {
  constructor() {
    super()

    this._embed = { type: 'rich', description: '', color: '', title: '', footer: {}, image: {}, video: {}, thumbnail: {}, author: {}, timestamp: '', provider: {}, fields: [] }
  }

  /**
   * Set a field of the embed object to a value
   *
   * @param {string} field The field to edit in the embed object
   * @param {string} value The value of the field
   */
  setField(field, value) {
    this._embed[field] = value

    return this
  }

  /**
   * Build the embed object with the data provided from the `setField` function
   */
  _embedBuilder() {
    // this._embed.description = this._embed.description ? this._embed.description : ''
    // this._embed.color = this._embed.color ? parseInt(this._embed.color, 16) : ''
    // this._embed.title = this._embed.title ? this._embed.title : ''
    // this._embed.footer.text = this._embed.footer.text ? this._embed.footer.text : ''
    // this._embed.footer.icon_url = this._embed.footer.icon_url ? this._embed.footer.icon_url : ''
    // this._embed.image.url = this._embed.image.url ? this._embed.image.url : ''
    // this._embed.video.url = this._embed.video.url ? this._embed.video.url : ''
    // this._embed.thumbnail.url = this._embed.thumbnail.url ? this._embed.thumbnail.url : ''
    // this._embed.author.name = this._embed.author.name ? this._embed.author.name : ''
    // this._embed.author.url = this._embed.author.url ? this._embed.author.url : ''
    // this._embed.author.icon_url = this._embed.author.icon_url ? this._embed.author.icon_url : ''
    // this._embed.timestamp = this._embed.timestamp ? this._embed.timestamp : ''
    // this._embed.provider.name = this._embed.provider.name ? this._embed.provider.name : ''
    // this._embed.provider.url = this._embed.provider.url ? this._embed.provider.url : ''
    // this._embed.field = this._embed.field ? this._embed.field : [{}]

    // this._embed.field.forEach(field => {
    //   field.name = field.name ? field.name : '';
    //   field.value = field.value ? field.value : '';
    //   field.inline = field.inline ? field.inline : ''
    // })
  }

  /**
   * Send an embed message
   *
   * @returns void
   */
  async send() {
    this._embedBuilder()

    await super._create({ embed: this._embed })
  }

  /**
   * Update the content of an embed message
   *
   * @param {string} id The snowflake id of the embed message to update
   * @returns void
   */
  async update(id) {
    this._embedBuilder()

    await super._update(id, { embed: this._embed })
  }
}

class DiscordEmbedMessageBuilder extends DiscordEmbedMessage {
  constructor() {
    super()
  }

  /**
   * Set the title of the embed message
   *
   * @param {string} title The title of the embed message
   * @example setTitle('Some title')
   */
  setTitle(title) {
    this._embed.title = title

    return this
  }

  /**
   * Adds a field to the embed (max 25).
   *
   * @param {string} name The name of the field
   * @param {string} value The value of the field
   * @param {boolean} inline Set the field to display inline
   * @example addField('Regular field title', 'Some value here')
   * @example addField('Inline field title', 'Some value here', true)
   */
  addField(name, value, inline = false) {
    if (this._embed.fields.length > 25) return this

    this._embed.fields.push({
      name: name,
      value: value,
      inline: inline
    })

    return this
  }

  /**
   * Sets the author of this embed.
   *
   * @param {string} name The name of the author
   * @param {string} icon The icon URL of the author
   * @param {string} url The URL of the author
   * @example setAuthor('Some name', 'https://i.imgur.com/wSTFkRM.png', 'https://discord.js.org')
   */
  setAuthor(name, icon = null, url = null) {}

  /**
   * Sets the color of this embed.
   *
   * @param {string} color The color of the embed message
   * @example setColor('0099ff')
   */
  setColor(color) {
    this._embed.color = parseInt(color, 16)

    return this
  }

  /**
   * Sets the description of this embed.
   *
   * @param {string} description The description
   * @example setDescription('Some description here')
   */
  setDescription(description) {
    this._embed.description = description

    return this
  }

  /**
   * Sets the footer of this embed.
   *
   * @param {string} text The text of the footer
   * @param {string} icon The icon URL of the footer
   * @example setFooter('Some footer text here', 'https://i.imgur.com/wSTFkRM.png')
   */
  setFooter(text, icon = null) {
    this._embed.footer.text = text

    if (icon.length > 1) this._embed.footer.icon_url = icon

    return this
  }

  /**
   * Set the image of this embed.
   *
   * @param {string} url The URL of the image
   * @example setImage('https://i.imgur.com/wSTFkRM.png')
   */
  setImage(url) {
    this._embed.image.url = url

    return this
  }

  /**
   * Set the thumbnail of this embed.
   *
   * @param {string} url The URL of the thumbnail
   * @example setThumbnail('https://i.imgur.com/wSTFkRM.png')
   */
  setThumbnail(url) {
    this._embed.thumbnail.url = url

    return this
  }
}

const builder = new DiscordEmbedMessageBuilder()
builder.login('TOKEN-HERE')
builder.setGuild(window.location.pathname.split('/').slice(2, 3).pop())
builder.setChannel(window.location.pathname.split('/').slice(2, 4).pop())
