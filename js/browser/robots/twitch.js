const aggregation = (baseClass, ...mixins) => {
  class base extends baseClass {
    constructor(...args) {
      super(...args)

      mixins.forEach(mixin => {
        copyProps(this, (new mixin))
      })
    }
  }

  let copyProps = (target, source) => {
    Object.getOwnPropertyNames(source)
      .concat(Object.getOwnPropertySymbols(source))
      .forEach(prop => {
        if (!prop.match(/^(?:constructor|prototype|arguments|caller|name|bind|call|apply|toString|length)$/))
          Object.defineProperty(target, prop, Object.getOwnPropertyDescriptor(source, prop));
      })
  }

  mixins.forEach(mixin => {
    copyProps(base.prototype, mixin.prototype)
    copyProps(base, mixin)
  })

  return base
}

class Logger {
  constructor(prefix, style) {
    this._prefix = prefix
    this._style = style
  }

  log(...args) {
    args[1] = args[1] ? args[1] : ''
    console.log.apply(null, [`${this._prefix} %c${args[0]}`, this._style, 'color:lightgrey', args[1]])
  }

  info(...args) {
    args[1] = args[1] ? args[1] : ''
    console.info.apply(null, [`${this._prefix} %c${args[0]}`, this._style, 'color:lightgrey', args[1]])
  }

  warn(...args) {
    args[1] = args[1] ? args[1] : ''
    console.warn.apply(null, [`${this._prefix} %c${args[0]}`, this._style, 'color:pale', args[1]])
  }

  debug(...args) {
    args[1] = args[1] ? args[1] : ''
    console.debug.apply(null, [`${this._prefix} %c${args[0]}`, this._style, 'color:pale', args[1]])
  }

  error(...args) {
    args[1] = args[1] ? args[1] : ''
    console.error.apply(null, [`${this._prefix} %c${args[0]}`, this._style, 'color:pale', args[1]])
  }
}

class TwitchAPI {
  constructor() {}

  /**
   * Init
   */
  init() {
    console.clear()

    this._ws = new WebSocket('wss://irc-ws.chat.twitch.tv')
    this._logger = new Logger('%c[Twitch]', 'color:#a02d2a;font-weight:bold')
    this._debug = true

    if (this._debug) this._logger.debug('Initialisation...')

    this._setup()
  }

  _setup() {
    setInterval(() => {
      if (this._debug) this._logger.debug('Sending a heartbeat to the server ("PING")')

      this._ws.send('PING')
    }, 3e5)

    this._ws.onopen = _ => this._onopen()
    this._ws.onmessage = e => this._onmessage(e)
    this._ws.onclose = e => this._onclose(e)
    this._ws.onerror = e => this._onerror(e)
  }

  _onopen() {
    if (this._error) return

    this._logger.info('Connection opened!')
    this._ws.send('CAP REQ :twitch.tv/tags twitch.tv/commands')

    this._oauth()
  }

  _oauth() {
    this._logger.info('Authentication...')

    this._ws.send(`PASS oauth:${this._pass}`)
    this._ws.send(`NICK ${this._nick}`)
    this._ws.send(`USER ${this._nick} 8 * :${this._nick}`)

    this._logger.info('Authenticated!')

    this._joinchannel(this._channel)
  }

  /**
   * @param {MessageEvent} e
   */
  _onmessage(e) {
    if (this._error) return

    this._data = e.data.replace('\n', '')

    if (this._debug) this._logger.debug(`Message received! Content: ${this._data}`)
  }

  /**
   * @param {CloseEvent} e
   */
  _onclose(e) {
    if (this._error) return

    this._parse(this._data)
    this._logger.warn(`Connection closed! Reason: %c${this._error}`, 'font-weight:bold')
  }

  /**
   * @param {Event} e
   */
  _onerror(e) {
    if (this._error) return

    this._error = true
    this._logger.error('Error!')
  }

  _parse(data) {
    if (!data.match(/\* \:([a-zA-Z ]+)/)) return
    if (!data.match(/\* \:([a-zA-Z ]+)/)[1]) return

    this._error = data.match(/\* \:([a-zA-Z ]+)/)[1]
  }
}

class TwitchData extends TwitchAPI {
  constructor() {
    super()

    setTimeout(() => {
      super._onmessage = e => this._decode(e.data)
    }, 25)
  }

  _decode(data) {
    this._logger.debug(`Message received! Content: ${data}`)

    const msgs = [{
      reg: 'PING',
      fn: () => this._ping(data)
    }, {
      reg: 'CAP *',
      fn: () => this._logger.log('Connexion accepted!')
    }, {
      reg: 'Welcome, GLHF!',
      fn: () => this._logger.log('Welcome message received!')
    }, {
      reg: 'GLOBALUSERSTATE',
      fn: () => this._logger.log('GLOBALUSERSTATE data loaded!')
    }, {
      reg: 'NOTICE *',
      fn: () => this._notice(data)
    }, {
      reg: 'JOIN',
      fn: () => this._join(data)
    }, {
      reg: 'PART',
      fn: () => this._part(data)
    }, {
      reg: 'End of',
      fn: () => this._logger.log('/names list received!')
    }, {
      reg: 'USERSTATE',
      fn: () => this._userstate(data)
    }, {
      reg: 'ROOMSTATE',
      fn: () => this._roomstate(data)
    }, {
      reg: 'PRIVMSG',
      fn: () => this._privmsg(data)
    }]

    msgs.forEach(msg => {
      if (!data.match(msg.reg)) return

      msg.fn(data)
    })
  }

  _joinchannel(channel) {
    this._ws.send(`JOIN #${channel}`)
  }

  _ping() {
    if (this._debug) this._logger.debug('Receiving a heartbeat from the server ("PING")')
    if (this._debug) this._logger.debug('Sending a heartbeat to the server ("PONG")')

    this._ws.send('PONG')
  }

  _notice(data) {
    const msg = data.match(/NOTICE \* :([a-zA-Z0-9 &é"'(-è_çà)=~#{[|`\\^@\]}€^$£ù%*µ,?;.:\/!§<>ê]+)/)[1]

    this._logger.log(msg)
  }

  _join(data) {
    const ch = data.match(/JOIN #([a-z0-9]+)/)[1]

    this._logger.log(`Channel ${ch} joined`)
  }

  _part(data) {
    const ch = data.match(/JOIN #([a-z0-9]+)/)[1]

    this._logger.log(`Channel ${ch} leaved`)
  }

  _userstate(data) {
    const mod = data.includes('moderator')
    const prime = data.includes('premium')
    const color = data.match(/color=(#[A-Z0-9]+)/)[1]
    const sub = !!Number(data.match(/subscriber=([0-9]+)/)[1])
    const name = data.match(/display-name=([a-zA-Z0-9]+);/)[1]

    this._logger.log(`User data loaded! (name: ${name}, isMod: ${mod}, isSub: ${sub}, isPrime: ${prime}, color: ${color})`)
  }

  _roomstate(data) {
    const emote = !!Number(data.match(/emote-only=([0-9]+)/)[1])
    const follow = !Number(data.match(/followers-only=(-?[0-9]+)/)[1])
    const slow = !!Number(data.match(/slow=([0-9]+)/)[1])
    const subs = !!Number(data.match(/subs-only=([0-9]+)/)[1])

    this._logger.log(`Room data loaded! (isEmote: ${emote}, isFOnly: ${follow}, isSlow: ${slow}, isSubs: ${subs})`)
  }

  _privmsg(data) {
    this._message = new TwitchMessage(data.match(/PRIVMSG #([a-z]+) :([a-zA-Z0-9 &é"'(-è_çà)=~#{[|`\\^@\]}€^$£ù%*µ,?;.:\/!§<>ê]+)/)[2])
    this._author = new TwitchUser(data.match(/display-name=([a-zA-Z0-9]+);/)[1])
    this._channel = new TwitchChannel(data.match(/PRIVMSG #([a-z]+)/)[1])
  }
}

class TwitchCommandManager extends TwitchData {
  constructor() {
    super()
  }

  /**
   * Set the prefix of the Client
   *
   * @param prefix {string}
   */
  setPrefix(prefix) {
    this._prefix = prefix
  }

  /**
   * Retrieve the prefix of the Client
   *
   * @returns {string}
   */
  getPrefix() {
    return this._prefix
  }

  /**
   * Retrieve the commands of the Client
   *
   * @returns {{ name: string, fn: Funcation }[]}
   */
  get cmds() {
    return this._cmds
  }

  setCmds(cmds) {
    this._cmds = cmds
  }

  /**
   * Add a command to the bot
   *
   * @param param0 {{ name: string, fn: Function }}
   */
  addCmd({ name, fn }) {
    this._cmds.push({ name: name, fn: fn })
  }

  /**
   * Delete a command to the bot
   *
   * @param name {string}
   */
  delCmd(name) {
    const id = this._cmds.indexOf(name)

    this._cmds.splice(id, 1)
  }
}

class TwitchUser extends TwitchData {
  constructor(author) {
    super()

    this._username = author
  }

  get username() {
    return this._username
  }
}

class TwitchMessage extends TwitchCommandManager {
  constructor(content) {
    super()

    this._content = content
  }

  /**
   * Get the message content
   *
   * @returns {string}
   */
  get content() {
    return this._content.replace(this.getPrefix(), '')
  }

  /**
   * Send a message
   *
   * @param text {string}
   */
  send(text) {
    this._ws.send(`PRIVMSG #${this._channel.name()} :${text}`)
  }

  /**
   * Reply to the message sender
   *
   * @param text {string}
   */
  reply(text) {
    this.send(`@${this._author.username()} ${text}`)
  }
}

class TwitchChannel extends TwitchData {
  constructor(channel) {
    super()

    this._channel = channel

    this._modes = new TwitchChannelModes(channel)
    this._actions = new TwitchChannelActions(channel)
  }

  /**
   * Returns the name of the channel
   *
   * @returns {string} The name of the channel
   */
  get name() {
    return this._channel
  }

  /**
   * Join and leave a channel
   *
   * @param channel {string} The channel who should be joined
   * @example join('kasaido')
   */
  join(channel) {
    if (this._channel) this._ws.send(`PART #${this._channel}`)

    this._channel = channel
    this._setup(channel)
  }
}

class TwitchChannelModes extends TwitchChannel {
  constructor(channel) {
    super(channel)
  }

  /**
   * Activate the "FollowersOnly" mode
   *
   * @param type {boolean} Activate or not the mode
   * @param time {number} How long should the mode be active?
   * @example follow(true, 500)
   * @example follow(false)
   */
  follow(type, time = 30) {
    this._message.send(`/followers${type ? ' ' : 'off'}${type ? time : ''}`);
  }

  /**
   * Activate the "Slow" mode
   *
   * @param type {boolean} Activate or not the mode
   * @example slow(true)
   * @example slow(false)
   */
  slow(type) {
    this._message.send(`/slow${type ? '' : 'off'}`);
  }
}

class TwitchChannelActions extends TwitchChannel {
  constructor(channel) {
    super(channel)
  }

  /**
   * Clear the channel
   */
  clear() {
    this._message.send('/clear')
  }

  /**
   * Set the color of the pseudo
   *
   * @param color {string} A hex string
   * @example setColor('#ff00ff')
   */
  setColor(color) {
    this._message.send(`/color ${color}`)
  }
}

class TwitchClient extends TwitchCommandManager {
  constructor({ nick, pass, channel, prefix = '!', cmds = [{ name, fn }] }) {
    super()

    this.setPrefix(prefix)
    this.setCmds(cmds)

    super._nick = nick
    super._pass = pass
    this._channel = channel
  }
}

const client = new TwitchClient({
  nick: '%USER%',
  pass: '%PASSWORD%',
  channel: '%CHANNEL%',
  prefix: '!',
  cmds: [{
    name: 'help',
    /**
     * @param client {TwitchClient}
     * @param channel {TwitchChannel}
     * @param message {TwitchMessage}
     * @param author {TwitchUser}
     */
    fn: (client, channel, message, author) => {
      const cmds = client.cmds.map(cmd => client.prefix + cmd.name).join(', ')

      message.reply(`Here are the available commands: ${cmds}`)
    }
  }, {
    name: 'test',
    /**
     * @param client {TwitchClient}
     * @param channel {TwitchChannel}
     * @param message {TwitchMessage}
     * @param author {TwitchUser}
     */
    fn: (client, channel, message, author) => message.send('ok')
  }]
})

client.destroy()
