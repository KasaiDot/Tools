const req = (userID, data) => {
  fetch(`https://discordapp.com/api/v6/guilds/YOUR-GUID-ID-HERE/members/${userID}`, {
    headers: {
      'Content-Type': 'application/json',
      authorization: 'YOUR-TOKEN-HERE'
    },
    body: data,
    method: 'PATCH'
  })
}

const switchUser = (userID, channelID) => {
  const json = JSON.stringify({
    channel_id: channelID
  })

  req(userID, json)
}

const settings = {
  userID: '252522849301037056',
  channelsID: ['643793061578539028', '643793127693615114']
}

const int = setInterval(() => {
  switchUser(settings.userID, settings.channelsID[0])

  setTimeout(() => {
    switchUser(settings.userID, settings.channelsID[1])
  }, 3e3)
}, 6e3)
