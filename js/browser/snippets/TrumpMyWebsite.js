javascript:(
  () => {
    const a = [
      'https://media.giphy.com/media/z48aJruaX0Jsk/giphy.gif',
      'https://media.giphy.com/media/lA1UjqWVdmmpq/giphy.gif',
      'https://media.giphy.com/media/26gseThXKNAhvef3G/giphy.gif',
      'https://media.giphy.com/media/l0HlDvNaJnsl7DR8A/giphy.gif',
      'https://media.giphy.com/media/3o7TKJgfPhqcck8elO/giphy.gif',
      'https://media.tenor.co/images/b8966c4d8f8b7ecba25f33f15f76aa0b/tenor.gif'
    ];
    let b = document.getElementsByTagName('img');
    for (let i = 0; i < b.length; i++) {
      b[i].src = a[Math.floor(Math.random(0) * a.length)];
    }
  }
)();
