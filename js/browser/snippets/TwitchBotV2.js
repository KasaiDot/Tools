class TwitchBot {
  constructor({ nick, pass, channel, prefix, cmds = [{ 'name': '', 'fn': '' }] }) {
    this.nick = nick;
    this.pass = pass;
    this.channel = channel;
    this.prefix = prefix;
    this.cmds = cmds;

    this.ws = new WebSocket('wss://irc-ws.chat.twitch.tv');
    this.ws.onopen = e => this._oauth();
    this.ws.onmessage = e => this._onmessage(e);
    setInterval(() => this.ws.send('PING'), 3e5);
  }

  _oauth() {
    this.ws.send('CAP REQ :twitch.tv/tags twitch.tv/commands');
    this.ws.send(`PASS ${this.pass}`);
    this.ws.send(`NICK ${this.nick}`);
    this.ws.send(`USER ${this.nick} 8 * :${this.nick}`);
    this._setup();
  }

  _setup() {
    this.ws.send(`JOIN #${this.channel}`);
  }

  _onmessage(e) {
    const { data } = e;
    if (new RegExp('\\bCAP *\\b').test(data)) console.log('Connexion accepted!');
    if (new RegExp('\\bWelcome, GLHF!\\b').test(data)) console.log('Welcome message received!');
    if (new RegExp('\\bGLOBALUSERSTATE\\b').test(data)) console.log('GlobalUser data loaded!');
    if (new RegExp('\\bNOTICE *\\b').test(data)) {
      const msg = data.match(/NOTICE \* :([a-zA-Z ]+)/)[1];
      console.log(msg);
    }
    if (new RegExp('\\bJOIN\\b').test(data)) {
      const ch = data.match(/JOIN #([a-z0-9]+)/)[1];
      console.log(`Channel ${ch} joined`);
    }
    if (new RegExp('\\bPART\\b').test(data)) {
      const ch = data.match(/JOIN #([a-z0-9]+)/)[1];
      console.log(`Channel ${ch} leaved`);
    }
    if (new RegExp('\\bEnd of\\b').test(data)) console.log('/names list received!');
    if (new RegExp('\\bUSERSTATE\\b').test(data)) {
      const mod = data.includes('moderator');
      const prime = data.includes('premium');
      const color = data.match(/color=(#[A-Z0-9]+)/)[1];
      const sub = !!Number(data.match(/subscriber=([0-9]+)/)[1]);
      const name = data.match(/display-name=([a-zA-Z0-9]+);/)[1];
      console.log(`User data loaded! (name: ${name}, isMod: ${mod}, isSub: ${sub}, isPrime: ${prime}, color: ${color})`);
    }
    if (new RegExp('\\bROOMSTATE\\b').test(data)) {
      const emote = !!Number(data.match(/emote-only=([0-9]+)/)[1]);
      const follow = !Number(data.match(/followers-only=(-?[0-9]+)/)[1]);
      const slow = !!Number(data.match(/slow=([0-9]+)/)[1]);
      const subs = !!Number(data.match(/subs-only=([0-9]+)/)[1]);
      console.log(`Room data loaded! (isEmote: ${emote}, isFOnly: ${follow}, isSlow: ${slow}, isSubs: ${subs})`);
    }
    if (new RegExp('\\bPRIVMSG\\b').test(data)) {
      const cmds = this.cmds;
      const author = data.match(/display-name=([a-zA-Z0-9]+);/)[1];
      const msg = data.match(/PRIVMSG #([a-z]+) :([a-zA-Z0-9 &é"'(-è_çà)=~#{[|`\\^@\]}€^$£ù%*µ,?;.:\/!§<>ê]+)/);
      console.log(`${author}@${msg[1]} send: ${msg[2]}`);
      cmds.forEach(cmd => msg.includes(this.prefix + cmd.name) ? cmd.fn() : null);
    }
  }

  setNick(nick) {
    this.nick = nick;
    this._oauth();
  }

  setPass(pass) {
    this.pass = pass;
    this._oauth();
  }

  setPrefix(prefix) {
    this.prefix = prefix;
  }

  addCmd({name, fn}) {
    this.cmds.push({'name': name, 'fn': fn});
  }

  delCmd(name) {
    const id = this.cmds.indexOf(name);
    this.cmds.splice(id, 1);
  }

  join(channel) {
    this.ws.send(`PART #${this.channel}`);
    this.channel = channel;
    this._setup();
  }

  send(text) {
    this.ws.send(`PRIVMSG #${this.channel} :${text}`);
  }
}
