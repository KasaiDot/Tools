const config = {
  discord: {
    apiV: 6,
    token: 'YOUR-TOKEN-HERE',
    channelId: [],
    userId: [],
  },
  msg: [
    `Hey {{user}} you suck!`
  ],
  time: 3e3,
}

const interval = setInterval(() => {
  const content = config.msg[Math.floor(Math.random() * config.msg.length)].replace('{{user}}', `<@${config.discord.userId[Math.floor(Math.random() * config.discord.userId.length)]}>`)
  fetch(`https://discordapp.com/api/v${config.discord.apiV}/channels/${config.discord.channelId[Math.floor(Math.random() * config.discord.channelId.length)]}/messages`, {
    method: 'POST',
    headers: {
      'Authorization': config.discord.token,
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ content })
  })
}, config.time)
