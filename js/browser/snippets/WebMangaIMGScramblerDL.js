const canvas = $0

const canvToBlob = () => {
  const bin = atob(canvas.toDataURL().split(',')[1])
  const arr = new Uint8Array(bin.length).map((v, i) => bin.charCodeAt(i))

  return new Blob([arr], {
    type: 'image/png'
  })
}

const doDownload = () => {
  const a = document.createElement('a')
  a.href = URL.createObjectURL(canvToBlob())
  a.download = 'canvas.png'
  a.click()
  URL.revokeObjectURL(a.href)
  a.removeAttribute('href')
}

doDownload()
