const chat = new WebSocket('wss://irc-ws.chat.twitch.tv');

chat.send('CAP REQ :twitch.tv/tags twitch.tv/commands');
chat.send('PASS oauth:YOUR-TOKEN-HERE');
chat.send('NICK kasaido');
chat.send('USER kasaido 8 * :kasaido');
chat.send('JOIN #alexmaw');

chat.onmessage = e => {
  if (e.data.includes('PING')) return chat.send('PONG');
  if (!e.data.includes('PRIVMSG')) return;
  const ifMod = e.data.includes('moderator');
  const ifPremium = e.data.includes('premium');
  const author = e.data.match(/display-name=([a-zA-Z0-9]+);/)[1];
  const msg = e.data.match(/PRIVMSG #alexmaw :([a-zA-Z0-9 &é"'(-è_çà)=~#{[|`\\^@\]}€^$£ù%*µ,?;.:\/!§<>ê]+)/)[1];

  if (msg.includes('!kasai')) chat.send("PRIVMSG #alexmaw :oui bonjour c'est moi que passa ?");

  console.log(`${author} is mod ? ${ifMod}`);
  console.log(`${author} is premium ? ${ifPremium}`);
  console.log(`${author} sended a message : ${msg}`);
};
