const config = {
    endpoint: 'https://wakatime.com/api',
    apiV: 1,
    token: 'YOUR-TOKEN-HERE'
}

async function req(method, endpoint, body = null) {
    try {
        const { json } = await fetch(`${config.endpoint}/v${config.apiV}/${endpoint}`, {
            headers: {
                accept: 'application/json, text/javascript, */*; q=0.01',
                'x-csrftoken': config.token
            },
            method,
            body
        })

        return await json()
    } catch (err) {
        console.error(err)
    }
}

async function deleteProject(project) {
    try {
        await req('DELETE', `users/current/projects/${project.name}`)
    } catch (err) {
        console.error(err)
    }
}

async function getAllProjects() {
    try {
        return await req('GET', 'users/current/projects?q=')
    } catch (err) {
        console.error(err)
    }
}

async function deleteAllProjects() {
    const projects = await getAllProjects()

    projects.data.forEach(deleteProject)
}
