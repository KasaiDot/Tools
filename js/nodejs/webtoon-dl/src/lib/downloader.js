const { createWriteStream, writeFileSync, link, mkdirSync, existsSync } = require('fs')
const { resolve } = require('path')
const axios = require('axios').default
const ProgressBar = require('progress')
const { eachLimit } = require('async')

const ManhwaDotClub = require('../sources/manhwa-club')
const KakaoPage = require('../sources/kakaopage')

const THREADS = 1

/**
 * Downloader class
 */
class Downloader {
    /**
     * 
     * @param param0 {{ website: KakaoPage | ManhwaDotClub, seriesId: string }}
     */
    constructor({ website, seriesId }) {
        this._website = website
        this._seriesId = seriesId
    }

    async getChapters() {
        try {
            const res = await this._website.getChapters(this._seriesId)

            return res
        } catch (err) {
            console.error(err)
        }
    }

    async getChapterImages(chapterId) {
        try {
            const res = await this._website.getChapter(chapterId)

            return res
        } catch (err) {
            console.error(err)
        }
    }

    _doDownload(images, cb) {
        const self = this

        eachLimit(images, THREADS, function (image, next) {
            axios({
                url: image.file.link,
                method: 'GET',
                headers: {
                    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:83.0) Gecko/20100101 Firefox/83.0',
                    'Referer': `${self._website._endpoint}`
                },
                responseType: 'stream'
            })
                .then(({ data, headers }) => {
                    const filename = `${image.file.id}.${image.file.ext}`
                    const totalLength = headers['content-length']
                    const progressBar = new ProgressBar(`Downloading File: ${filename} Chapter: ${image.id} (${image.title}) Manhwa: ${self._seriesId} | [:bar] :percent :etas`, {
                        width: 20,
                        complete: '=',
                        incomplete: ' ',
                        renderThrottle: 1,
                        total: parseInt(totalLength, 10)
                    })

                    let path = resolve('Webtoons')

                    if (!existsSync(path)) {
                        mkdirSync(path)
                    }

                    path = resolve(path, self._website._name)

                    if (!existsSync(path)) {
                        mkdirSync(path)
                    }

                    path = resolve(path, self._seriesId)

                    if (!existsSync(path)) {
                        mkdirSync(path)
                    }

                    path = resolve(path, `Chapter ${image.id}`)

                    if (!existsSync(path)) {
                        mkdirSync(path)
                    }

                    writeFileSync(resolve(path, filename), '')

                    const stream = createWriteStream(resolve(path, filename))

                    data.on('data', chunk => progressBar.tick(chunk.length))
                    data.pipe(stream)
                    data.on('end', () => {
                        next()
                    })
                    data.on('error', error => {
                        console.log('Download error')
                        console.log(error)
                    })
                })
                .catch(err => {
                    console.error(err.message)
                })
        }, function () {
            cb()
        })
    }

    async init() {
        try {
            const chapters = await this.getChapters()

            chapters.forEach(async chapter => {
                const images = await this.getChapterImages(chapter)

                this._doDownload(images, () => {
                    console.log(`Chapter ${chapter.id} has finished downloading!`)
                })
            })

            console.log('finished!')
        } catch (err) {
            console.error(err)
        }
    }
}

module.exports = Downloader
