const axios = require('axios').default
const { load } = require('cheerio')

class Toonily {
    constructor() {
        this._name = 'Toonily'

        this._isApi = false
        this._endpoint = 'https://toonily.com/'
        this._endpoint_2 = 'https://toonily.com/webtoon'

        this._seriesId = ''
        this._productId = ''
    }

    async _request(type, endpoint, content = '') {
        try {
            const { data } = await axios({
                url: `${this._endpoint_2}/${endpoint}`,
                method: type,
                headers: {
                    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:83.0) Gecko/20100101 Firefox/83.0',
                    'Referer': 'https://toonily.com/'
                },
                data: content
            })

            return data
        } catch (err) {
            console.error(err)
        }
    }

    async getChapters(seriesId) {
        try {
            this._seriesId = seriesId

            const html = await this._request('GET', seriesId)
            const $ = load(html)
            const tab = []
            const self = this

            $('.wp-manga-chapter').each(function (i, el) {
                const link = $(this).find('a')

                tab.push({
                    id: link.text().replace('\n', '').replace('Chapter', '').trim(),
                    title: link.text().replace('\n', '').trim(),
                    link: link.attr('href').replace(`${self._endpoint_2}/`, '').slice(0, -1)
                })
            })

            tab.reverse()

            return tab
        } catch (err) {
            console.error(err)
        }
    }

    async getChapter(product) {
        try {
            this._productId = product.id

            const html = await this._request('GET', product.link)
            const $ = load(html)
            const tab = []

            $('.wp-manga-chapter-img').each((_, el) => {
                let link

                if (el.attribs['data-src'])
                    link = el.attribs['data-src'].trim()
                else
                    link = el.attribs['src'].trim()

                const filename = link.split('/').pop()

                const id = filename.split('.').shift()
                const ext = filename.split('.').pop()

                tab.push({
                    id: product.id,
                    title: product.title,
                    file: {
                        id,
                        ext,
                        link
                    }
                })
            })

            return tab
        } catch (err) {
            console.error(err)
        }
    }
}

module.exports = Toonily
